#include<iostream>
#include<fstream>
#include"Smallest.h"
#include"Product.h"
#include"Sum.h"
#include"Average.h"

using namespace std;

int main() {
	const int size = 50;
	int array[size];
	int summ = 0;
	int produc = 1;

	ifstream file("input.txt");

	if (file.fail()) {
		cout << "input.txt does not exists." << endl;
		return 0;
	}

	int count = 0;
	int x;

	while (count < size && file >> x) {
		array[count++] = x;
	}

	sum(summ, array, array[0]);
	product(produc, array, array[0]);
	average(summ, array, array[0]);
	smallest(summ, array, array[0]);

	file.close();

	return 0;
}