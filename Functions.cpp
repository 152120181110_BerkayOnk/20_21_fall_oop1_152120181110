#include <iostream>
#include <cstdio>
using namespace std;

/*
Add `int max_of_four(int a, int b, int c, int d)` here.
*/
int max_of_four(int a, int b, int c, int d) {
    if (a > b && a > c && a > d) {
        cout << a << endl;
    }
    else if (b > a && b > c && b > d) {
        cout << b << endl;
    }
    else if (c > a && c > b && c > d) {
        cout << c << endl;
    }
    else {
        cout << d << endl;
    }
    exit(0);
}

int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}