#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int a;
    cin >> a;

    int b[a];
    for (int i = 0; i < a; i++) {
        cin >> b[i];
    }

    for (int z = a - 1; z >= 0; z--) {
        cout << b[z] << " ";
    }

    return 0;
}