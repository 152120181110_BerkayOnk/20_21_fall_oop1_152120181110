#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */

    int a;
    int b;
    cin >> a;
    cin >> b;

    int* z[a];
    int t = 0;
    while (a--) {
        int e;
        cin >> e;

        z[t] = new int[e];
        for (int i = 0; i < e; i++) {
            cin >> z[t][i];
        }
        t++;
    }
    while (b--) {
        int k;
        int l;
        cin >> k;
        cin >> l;

        cout << z[k][l] << endl;
    }

    return 0;
}