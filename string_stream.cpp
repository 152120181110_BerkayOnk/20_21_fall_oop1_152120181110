#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    // Complete this function
    vector<int> a;
    stringstream ss(str);

    int z;
    char x;

    while (ss) {
        if (ss.peek() != ',') {
            if (ss >> z) {
                a.push_back(z);
            }
        }
        else {
            ss >> x;
        }
    }
    return a;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}